home = 'D:\Dropbox\Work\TrustRegionPICE\vcode\';

kl_trpo_total_cost = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_18_15_01_0001\progress.csv'],'Total cost');
kl_trpo_avg_return = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_18_15_01_0001\progress.csv'],'AverageReturn');
npireps_0_3_total_cost = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_18_54_45_0001\progress.csv'],'Total cost');
npireps_0_3_avg_return = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_18_54_45_0001\progress.csv'],'AverageReturn');
npireps_0_1_total_cost = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_06_25_0001\progress.csv'],'Total cost');
%experiment_2017_02_23_18_26_08_0001
npireps_0_1_avg_return = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_06_25_0001\progress.csv'],'AverageReturn');
npireps_0_2_total_cost = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_18_52_0001\progress.csv'],'Total cost');
npireps_0_2_avg_return = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_18_52_0001\progress.csv'],'AverageReturn');
npireps_0_4_total_cost = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_19_50_55_0001\progress.csv'],'Total cost');
npireps_0_4_avg_return = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_19_50_55_0001\progress.csv'],'AverageReturn');
npireps_0_05_total_cost_eps_0_05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_19_55_44_0001\progress.csv'],'Total cost');
npireps_0_05_avg_return_eps_0_05  = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_19_55_44_0001\progress.csv'],'AverageReturn');
npireps_0_05_total_cost_eps_0_01 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_16_29_0001\progress.csv'],'Total cost');
npireps_0_05_avg_return_eps_0_01  = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_16_29_0001\progress.csv'],'AverageReturn');

npireps_0_3_total_cost_2N = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_21_05_16_0001\progress.csv'],'Total cost');
npireps_0_3_avg_return_2N = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_21_05_16_0001\progress.csv'],'AverageReturn');
npireps_0_2_total_cost_2N = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_36_35_0001\progress.csv'],'Total cost');
npireps_0_2_avg_return_2N = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_36_35_0001\progress.csv'],'AverageReturn');
npireps_0_25_total_cost_2N = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_21_05_18_0001\progress.csv'],'Total cost');
npireps_0_25_avg_return_2N = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_21_05_18_0001\progress.csv'],'AverageReturn');
npireps_0_1_total_cost_2N = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_37_22_0001\progress.csv'],'Total cost');
npireps_0_1_avg_return_2N = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_20_37_22_0001\progress.csv'],'AverageReturn');

kl_trpo_total_cost_unc05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_11_58_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_unc05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_11_58_0001\progress.csv'],'AverageReturn');
npireps_0_05_total_cost_unc05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_17_38_0001\progress.csv'],'Total cost');
npireps_0_05_avg_return_unc05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_17_38_0001\progress.csv'],'AverageReturn');
npireps_0_1_total_cost_unc05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_12_51_0001\progress.csv'],'Total cost');
npireps_0_1_avg_return_unc05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_12_51_0001\progress.csv'],'AverageReturn');
npireps_0_15_total_cost_unc05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_21_21_0001\progress.csv'],'Total cost');
npireps_0_15_avg_return_unc05 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_21_21_0001\progress.csv'],'AverageReturn');

kl_trpo_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_52_50_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_52_50_0001\progress.csv'],'AverageReturn');

npireps_0_05_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_36_05_0001\progress.csv'],'Total cost');
npireps_0_05_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_36_05_0001\progress.csv'],'AverageReturn');

npireps_0_1_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_43_40_0001\progress.csv'],'Total cost');
npireps_0_1_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_43_40_0001\progress.csv'],'AverageReturn');

npireps_0_2_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_36_48_0001\progress.csv'],'Total cost');
npireps_0_2_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_36_48_0001\progress.csv'],'AverageReturn');

npireps_0_3_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_44_14_0001\progress.csv'],'Total cost');
npireps_0_3_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_23_22_44_14_0001\progress.csv'],'AverageReturn');

kl_trpo_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_12_37_15_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_12_37_15_0001\progress.csv'],'AverageReturn');
npireps_0_01_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_12_37_47_0001\progress.csv'],'Total cost');
npireps_0_01_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_12_37_47_0001\progress.csv'],'AverageReturn');
npireps_0_02_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_12_49_42_0001\progress.csv'],'Total cost');
npireps_0_02_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_12_49_42_0001\progress.csv'],'AverageReturn');
npireps_0_05_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_12_49_50_0001\progress.csv'],'Total cost');
npireps_0_05_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_12_49_50_0001\progress.csv'],'AverageReturn');

kl_trpo_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_16_45_01_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_16_45_01_0001\progress.csv'],'AverageReturn');
kl_trpo_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_16_58_54_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_16_58_54_0001\progress.csv'],'AverageReturn');
kl_trpo_total_cost_unc05_lambda2_std = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_18_16_35_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_unc05_lambda2_std = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_18_16_35_0001\progress.csv'],'AverageReturn');

kl_trpo_total_cost_lambda1_nod_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_21_20_54_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_lambda1_nod_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_21_20_54_0001\progress.csv'],'AverageReturn');
kl_trpo_total_cost_lambda1_nod_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_21_21_23_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_lambda1_nod_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_21_21_23_0001\progress.csv'],'AverageReturn');

kl_trpo_total_cost_lambda1_std_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_34_13_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_lambda1_std_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_34_13_0001\progress.csv'],'AverageReturn');
kl_trpo_total_cost_lambda1_std_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_34_26_0001\progress.csv'],'Total cost');
kl_trpo_avg_return_lambda1_std_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_34_26_0001\progress.csv'],'AverageReturn');

kl_trpo_total_cost_lambda1_std_s3 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_50_12_0001\progress.csv'],'Total cost');
kl_trpo_total_cost_lambda1_std_s4 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_50_19_0001\progress.csv'],'Total cost');

kl_trpo_total_cost_lambda1_w50_std_s3 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_23_15_03_0001\progress.csv'],'Total cost');
kl_trpo_total_cost_lambda1_w50_std_s4 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_23_15_34_0001\progress.csv'],'Total cost');

dtrpo = [kl_trpo_total_cost_lambda1_std_s1 kl_trpo_total_cost_lambda1_w50_std_s3 kl_trpo_total_cost_lambda1_w50_std_s4];
kltrpo_mean = mean(dtrpo,2);
kltrpo_std = std(dtrpo,[],2);

npireps_0_001_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_17_55_34_0001\progress.csv'],'Total cost');
npireps_0_001_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_17_55_34_0001\progress.csv'],'AverageReturn');
npireps_0_01_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_34_26_0001\progress.csv'],'Total cost');
npireps_0_01_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_34_26_0001\progress.csv'],'AverageReturn');
% npireps_0_02_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_15_03_48_0001\progress.csv'],'Total cost');
% npireps_0_02_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_15_03_48_0001\progress.csv'],'AverageReturn');
% npireps_0_05_total_cost_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_15_05_46_0001\progress.csv'],'Total cost');
% npireps_0_05_avg_return_unc05_lambda2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_15_05_46_0001\progress.csv'],'AverageReturn');

npireps_0_01_total_cost_lambda1_std_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_04_55_0001\progress.csv'],'Total cost');
npireps_0_01_avg_return_lambda1_std_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_04_55_0001\progress.csv'],'AverageReturn');
npireps_0_1_total_cost_lambda1_std_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_04_59_0001\progress.csv'],'Total cost');
npireps_0_1_avg_return_lambda1_std_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_04_59_0001\progress.csv'],'AverageReturn');
npireps_0_01_total_cost_lambda1_std_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_20_31_0001\progress.csv'],'Total cost');
npireps_0_01_avg_return_lambda1_std_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_20_31_0001\progress.csv'],'AverageReturn');
npireps_0_1_total_cost_lambda1_std_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_20_11_0001\progress.csv'],'Total cost');
npireps_0_1_avg_return_lambda1_std_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_22_20_11_0001\progress.csv'],'AverageReturn');
%2017_02_24_14_53
%2017_02_24_14_53_20
npireps_0_2_total_cost_lambda1_std_s1 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_23_01_09_0001\progress.csv'],'Total cost');
npireps_0_2_total_cost_lambda1_std_s2 = get_data([home '\rllab\data\local\experiment\experiment_2017_02_24_23_00_58_0001\progress.csv'],'Total cost');

npireps_0_01_mean = mean([npireps_0_01_total_cost_lambda1_std_s1 npireps_0_01_total_cost_lambda1_std_s2],2);
npireps_0_01_std = std([npireps_0_01_total_cost_lambda1_std_s1 npireps_0_01_total_cost_lambda1_std_s2],[],2);
npireps_0_1_mean = mean([npireps_0_1_total_cost_lambda1_std_s1 npireps_0_1_total_cost_lambda1_std_s2],2);
npireps_0_1_std = std([npireps_0_1_total_cost_lambda1_std_s1 npireps_0_1_total_cost_lambda1_std_s2],[],2);
npireps_0_2_mean = mean([npireps_0_2_total_cost_lambda1_std_s1 npireps_0_2_total_cost_lambda1_std_s2],2);
npireps_0_2_std = std([npireps_0_2_total_cost_lambda1_std_s1 npireps_0_2_total_cost_lambda1_std_s2],[],2);


fig_width_pt = 300                      % Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/96                 % Convert pt to inch
golden_mean = (sqrt(5)-1.0)/2.0         % Aesthetic ratio
fig_width = fig_width_pt*inches_per_pt  % width in inches
fig_height = fig_width*golden_mean      % height in inches
h = figure(1);

clf;
errorbar(kltrpo_mean,kltrpo_std,'b','Linewidth',2)
hold on;
errorbar(npireps_0_01_mean,npireps_0_01_std,'r','Linewidth',2)
errorbar(npireps_0_1_mean,npireps_0_1_std,'g','Linewidth',2)
errorbar(npireps_0_2_mean,npireps_0_1_std,'m','Linewidth',2)



legend('KL-TRPO','Nat-PIREPS \delta = 0.01','Nat-PIREPS \delta = 0.1','Nat-PIREPS \delta = 0.2')
ylabel('Costs','fontsize',20)
xlabel('Iterations','fontsize',20)

set(gca,'xlim',[0 100])
set(findall(gcf,'-property','FontSize'),'FontSize',30)
set(h,'Units','Inches','Position',[100,100,100+fig_width,100+fig_height]);
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'fig5.pdf','-dpdf','-r0')

%saveas(h,'fig5.pdf') %,'psc2')

h = figure(2);

clf

% blue = kl_trpo
% green = good pireps 0.1
% yellow = 0.01
% red = 0.2

%subplot(2,1,1);
%plot(kl_trpo_total_cost_unc05_lambda2);
hold on;
%plot(kl_trpo_total_cost_unc05_lambda2_std);
%plot(kl_trpo_total_cost_lambda1_nod_s1);
%plot(kl_trpo_total_cost_lambda1_nod_s2);
plot(kl_trpo_total_cost_lambda1_std_s1);
%plot(kl_trpo_total_cost_lambda1_std_s2);
plot(kl_trpo_total_cost_lambda1_w50_std_s3);
plot(kl_trpo_total_cost_lambda1_w50_std_s4);

%plot(kl_trpo_total_cost_lambda1_std_s3);
%plot(kl_trpo_total_cost_lambda1_std_s4);
%plot(npireps_0_001_total_cost_unc05_lambda2);
plot(npireps_0_01_total_cost_lambda1_std_s1);
plot(npireps_0_01_total_cost_lambda1_std_s2);
plot(npireps_0_1_total_cost_lambda1_std_s1);
plot(npireps_0_1_total_cost_lambda1_std_s2);
plot(npireps_0_2_total_cost_lambda1_std_s1);
plot(npireps_0_2_total_cost_lambda1_std_s2);
% plot(npireps_0_02_total_cost_unc05_lambda2);
% plot(npireps_0_05_total_cost_unc05_lambda2);
grid on;
ylabel('Total Cost');
title('N = 400')
set(gca,'xlim',[0 100])
legend('KL-TRPO std1','KL-TRPO w50 std2','KL-TRPO w50 std2','Nat-PIREPS \delta=0.01','Nat-PIREPS \delta=0.01','Nat-PIREPS \delta=0.1','Nat-PIREPS \delta=0.1','Nat-PIREPS \delta=0.2','Nat-PIREPS \delta=0.2','location','northeast')

% subplot(2,1,2);
% %plot(kl_trpo_avg_return_unc05_lambda2);
% hold on;
% %plot(kl_trpo_avg_return_unc05_lambda2_std);
% %plot(kl_trpo_avg_return_lambda1_nod_s1);
% %plot(kl_trpo_avg_return_lambda1_nod_s2);
% plot(kl_trpo_avg_return_lambda1_std_s1);
% plot(kl_trpo_avg_return_lambda1_std_s2);
% %plot(npireps_0_001_avg_return_unc05_lambda2);
% plot(npireps_0_01_avg_return_lambda1_std_s1);
% plot(npireps_0_01_avg_return_lambda1_std_s2);
% plot(npireps_0_1_avg_return_lambda1_std_s1);
% plot(npireps_0_1_avg_return_lambda1_std_s2);
% % plot(npireps_0_02_avg_return_unc05_lambda2);
% % plot(npireps_0_05_avg_return_unc05_lambda2);
% ylabel('Average Reward');
% set(gca,'xlim',[0 100])
% 
% 
% grid on;
% %legend('KL-TRPO min-mean','KL-TRPO mean-std','KL-TRPO nodiv','KL-TRPO nodiv','Nat-PIREPS \delta=0.001','Nat-PIREPS \delta=0.01','Nat-PIREPS \delta=0.05','location','southeast')
% legend('KL-TRPO std1','KL-TRPO std2','Nat-PIREPS \delta=0.01','Nat-PIREPS \delta=0.01','Nat-PIREPS \delta=0.1','Nat-PIREPS \delta=0.1','Nat-PIREPS \delta=0.2','Nat-PIREPS \delta=0.2','location','southeast')
saveas(h,'fig2.eps','psc2')

% 
% 
% h = figure(2);
% 
% clf;
% 
% subplot(1,2,1);
% plot(kl_trpo_total_cost_unc05);
% hold on;
% plot(npireps_0_05_total_cost_unc05);
% plot(npireps_0_1_total_cost_unc05);
% plot(npireps_0_15_total_cost_unc05);
% grid on;
% ylabel('Total Cost');
% title('N = 400')
% 
% subplot(1,2,2);
% plot(kl_trpo_avg_return_unc05)
% ylabel('Average Reward');
% hold on;
% plot(npireps_0_05_avg_return_unc05);
% plot(npireps_0_1_avg_return_unc05);
% plot(npireps_0_15_avg_return_unc05);
% 
% grid on;
% legend('KL-TRPO','Nat-PIREPS \delta=0.05','Nat-PIREPS \delta=0.1','Nat-PIREPS \delta=0.15','Nat-PIREPS \delta=0.3','location','southeast')
% saveas(h,'fig2.eps','psc2')
% 
% 
% 
% h = figure(3);
% 
% clf;
% 
% subplot(1,2,1);
% plot(kl_trpo_total_cost);
% hold on;
% plot(npireps_0_1_total_cost_2N);
% plot(npireps_0_2_total_cost_2N);
% plot(npireps_0_25_total_cost_2N);
% plot(npireps_0_3_total_cost_2N);
% grid on;
% ylabel('Total Cost');
% title('N = 400')
% 
% subplot(1,2,2);
% plot(kl_trpo_avg_return)
% ylabel('Average Reward');
% hold on;
% plot(npireps_0_1_avg_return_2N);
% plot(npireps_0_2_avg_return_2N);
% plot(npireps_0_25_avg_return_2N);
% plot(npireps_0_3_avg_return_2N);
% 
% grid on;
% legend('KL-TRPO','Nat-PIREPS \delta=0.1','Nat-PIREPS \delta=0.2','Nat-PIREPS \delta=0.25','Nat-PIREPS \delta=0.3','location','southeast')
% saveas(h,'fig3.eps','psc2')
% 
% 
% 
% h = figure(4);
% 
% clf;
% 
% subplot(1,2,1);
% plot(kl_trpo_total_cost);
% hold on;
% plot(npireps_0_4_total_cost);
% plot(npireps_0_3_total_cost);
% plot(npireps_0_1_total_cost);
% plot(npireps_0_2_total_cost);
% plot(npireps_0_05_total_cost_eps_0_05);
% plot(npireps_0_05_total_cost_eps_0_01);
% grid on;
% ylabel('Total Cost');
% title('N = 200')
% 
% subplot(1,2,2);
% plot(kl_trpo_avg_return)
% ylabel('Average Reward');
% hold on;
% plot(npireps_0_4_avg_return);
% plot(npireps_0_3_avg_return);
% plot(npireps_0_1_avg_return);
% plot(npireps_0_2_avg_return);
% plot(npireps_0_05_avg_return_eps_0_05);
% plot(npireps_0_05_avg_return_eps_0_01);
% 
% grid on;
% legend('KL-TRPO','Nat-PIREPS \delta=0.4','Nat-PIREPS \delta=0.3','Nat-PIREPS \delta=0.2','Nat-PIREPS \delta=0.1','Nat-PIREPS \delta=0.05, \epsilon=0.05','Nat-PIREPS \delta=0.05, \epsilon=0.01','location','southeast')
% saveas(h,'fig4.eps','psc2')
% 
% 
